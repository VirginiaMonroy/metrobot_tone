# -*- coding: utf-8 -*-
# Copyright 2018 IBM Corp. All Rights Reserved.

# Licensed under the Apache License, Version 2.0 (the “License”)
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an “AS IS” BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import os
from dotenv import load_dotenv, find_dotenv
from flask import Flask, Response
from flask import jsonify
from flask import request, redirect
from flask_socketio import SocketIO
from flask_cors import CORS
from ibm_watson import AssistantV1
from ibm_watson import SpeechToTextV1
from ibm_watson import TextToSpeechV1
from ibm_watson import ToneAnalyzerV3
from ibm_watson import LanguageTranslatorV3
from ibm_cloud_sdk_core import get_authenticator_from_environment
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
import assistant_setup

## Declarando variables globales para los condicionales de la db
r = 0
t = 0
s = 0
n = 0
l = 0
c = 0
re = 0
g = 0
nombre = ''
location = ''
categoria = ''
rango_edad = ''
genero_persona = ''
review = ''
tone_name = ''
score = 1   
#####################################################################
# Accediendo a la base de datos
from pymongo import MongoClient
import dns

client = MongoClient("mongodb+srv://dbVirginia:VirginiaMonroy1013@cluster0-ufdhd.mongodb.net/test?retryWrites=true&w=majority")
db = client.get_database('metro')
reviews = db.reviews


# import tone detection
import tone_detection

app = Flask(__name__)
socketio = SocketIO(app)
CORS(app)

respuestas = []

# replace with your own tone analyzer credentials
tone_analyzer_authenticator = IAMAuthenticator(os.environ.get('TONE_ANALYZER_APIKEY') or '5lP22FrMcaS74l7M3YBon_NA6_Y79INgb5TBrBVhwFyx')
tone_analyzer = ToneAnalyzerV3(
    version='2016-05-19',
    authenticator=tone_analyzer_authenticator)

# translator
translator_authenticator = IAMAuthenticator('c_170tp9_7LaA-FBqtXyZJNG1iQpMASjjweCm5JvFwbF')
language_translator = LanguageTranslatorV3(
    version='2018-05-01',
    authenticator=translator_authenticator)
language_translator.set_service_url('https://gateway.watsonplatform.net/language-translator/api')

# This example stores tone for each user utterance in conversation context.
# Change this to false, if you do not want to maintain history
# replace with your own workspace_id
#workspace_id = os.environ.get('WORKSPACE_ID') or '739579f8-c411-4b6f-b7f2-b0a5749a64fb'
global_maintainToneHistoryInContext = True
#global_payload = {
#    'workspace_id': workspace_id,
#    'input': {
#        'text': "Me llamo Alexandra quiero comprar un polo para mi hijito que cumple tres años y le gusta la ropa de color oscuro"
#    }
#}

## Función del Tone Analyzer
def invokeToneConversation(payload, maintainToneHistoryInContext):
    ## Jalando las variables locales:
    global r 
    global t 
    global s 
    global n 
    global l 
    global c 
    global re
    global g
    global nombre
    global location
    global categoria
    global rango_edad
    global genero_persona
    global review
    global tone_name
    global score
    
    print("Entra aquí para el input")
    # Aquí entra si text es diferente de None y si es mayor a 40 la long porque es una review
    if payload['input']['text'] is not None and len(payload['intents']) != 0:
        if (payload['intents'][0]['intent']) == 'consulta':
            # Guardando la review
            review_es = payload['input']['text']
            ## traduciendo de español a inglés
            ## Translate
            translation = language_translator.translate(
                text=payload['input']['text'], model_id='es-en').get_result()
            input_translate = translation['translations'][0]['translation']
            #print(input_translate)
            payload['input']['text'] = input_translate
            print("Este es el payload traducidooooo")
            print(payload)
            ## Tone Analyzer
            tone = tone_analyzer.tone(tone_input=payload['input'], content_type='application/json').get_result()
            conversation_payload = tone_detection.\
                updateUserTone(payload, tone, maintainToneHistoryInContext)
            response_tone = assistant.message(workspace_id=workspace_id,
                                        input=conversation_payload['input'],
                                        context=conversation_payload['context']).get_result()
            print(json.dumps(response_tone, indent=2))

            #print("En el response hayyyyyyyyyy")
            #print(response_tone)
            #review = response_tone['input']['text']
            #print(review)
            tone_name = response_tone['context']['user']['tone']['emotion']['history'][0]['tone_name']
            #print(tone_name)
            score = response_tone['context']['user']['tone']['emotion']['history'][0]['score']
            #print(score)
            r = 1
            t = 1
            s = 1
    elif len(payload['entities']) != 0:
        print("El len de entities es diferente de 0")
        if payload['input']['text'] is not None and payload['entities'][0]['entity'] == 'nombres':
            # Guardando el nombre
            print("Guarda nombre")
            nombre = payload['entities'][0]['value']
            n = 1
        elif payload['input']['text'] is not None and payload['entities'][0]['entity'] == 'location':
            # Guardando location
            print("Guarda location")
            location = payload['entities'][0]['value']
            l = 1
        elif payload['input']['text'] is not None and payload['entities'][0]['entity'] == 'categoria':
            # Guardando categoria
            print("Guarda categoria")
            categoria = payload['entities'][0]['value']
            c = 1
        elif payload['input']['text'] is not None and payload['entities'][0]['entity'] == 'rango_edad':
            # Guardando rango_edad
            print("Guarda rango_edad")
            rango_edad = payload['entities'][0]['value']
            re = 1
        elif payload['input']['text'] is not None and payload['entities'][0]['entity'] == 'genero_persona':
            # Guardando genero_persona
            print("Guarda genero_persona")
            genero_persona = payload['entities'][0]['value']
            g = 1
        else:
            print("No guardé nada en las variables con el entity :(")
    else:
        print("No hay ningún input")

    ## Para guardar en la db
    if payload['input']['text'] is not None and len(payload['intents']) != 0:
        if (payload['intents'][0]['intent']) == 'consulta':
            print("Entra para guardar en la db...")
            #restricciones si hay valores nulos
            if r ==0: 
                 review = 'None'
            if t ==0: 
                tone_name = 'None'
            if s ==0: 
                score = 0
            if n ==0: 
                 nombre = 'None'
            if l ==0: 
                 location = 'None'
            if c ==0: 
                categoria = 'None'
            if re ==0:
                  rango_edad = 'None'
            if g ==0: 
                  genero_persona = 'None'

            print("Ya va a guardar")
            new_review = {
                'nombre': nombre,
                'location': location,
                'categoria': categoria,
                'rango_edad': rango_edad,
                'genero_persona': genero_persona,
                'review': review_es,
                'tone_name': tone_name,
                'score': score
            }
            ## Gurdando en la base de datos
            reviews.insert_one(new_review)
            print("Guardó")

# Redirect http to https on CloudFoundry
@app.before_request
def before_request():
    fwd = request.headers.get('x-forwarded-proto')

    # Not on Cloud Foundry
    if fwd is None:
        return None
    # On Cloud Foundry and is https
    elif fwd == "https":
        return None
    # On Cloud Foundry and is http, then redirect
    elif fwd == "http":
        url = request.url.replace('http://', 'https://', 1)
        code = 301
        return redirect(url, code=code)


@app.route('/')
def Welcome():
    return app.send_static_file('index.html')


@app.route('/api/conversation', methods=['POST', 'GET'])
def getConvResponse():

    convText = request.form.get('convText')
    convContext = request.form.get('context', "{}")
    jsonContext = json.loads(convContext)

    response = assistant.message(workspace_id=workspace_id,
                                 input={'text': convText},
                                 context=jsonContext)

    response = response.get_result()
    print("Voy a imprimir response")
    print(response)
    reponseText = response["output"]["text"]
    #print("Voy a imprimir reponseText")
    #print(reponseText)
    responseDetails = {'responseText': '... '.join(reponseText),
                       'context': response["context"]}
    #print("Voy a imprimir responseDetails")
    #print(responseDetails)
    #respuestas.append(response)
    # synchronous call to conversation with tone included in the context
    #print("Va a entrar a la función....")
    invokeToneConversation(response, global_maintainToneHistoryInContext)
    return jsonify(results=responseDetails)


@app.route('/api/text-to-speech', methods=['POST'])
def getSpeechFromText():
    inputText = request.form.get('text')
    ttsService = TextToSpeechV1()

    def generate():
        if inputText:
            audioOut = ttsService.synthesize(
                inputText,
                accept='audio/wav',
                voice='es-LA_SofiaVoice').get_result()

            data = audioOut.content
        else:
            print("Empty response")
            data = "I have no response to that."

        yield data

    return Response(response=generate(), mimetype="audio/x-wav")


@app.route('/api/speech-to-text', methods=['POST'])
def getTextFromSpeech():

    sttService = SpeechToTextV1()

    response = sttService.recognize(
            audio=request.get_data(cache=False),
            model='es-PE_BroadbandModel',
            content_type='audio/wav',
            timestamps=True,
            word_confidence=True,
            smart_formatting=True).get_result()

    # Ask user to repeat if STT can't transcribe the speech
    if len(response['results']) < 1:
        return Response(mimetype='plain/text',
                        response="Podría repetir lo que dijo, por favor")

    text_output = response['results'][0]['alternatives'][0]['transcript']
    text_output = text_output.strip()
    return Response(response=text_output, mimetype='plain/text')


port = os.environ.get("PORT") or os.environ.get("VCAP_APP_PORT") or 5000
if __name__ == "__main__":
    load_dotenv()
    # SDK is currently confused. Only sees 'conversation' for CloudFoundry.
    authenticator = (get_authenticator_from_environment('assistant') or
                     get_authenticator_from_environment('conversation'))
    assistant = AssistantV1(version="2019-11-06", authenticator=authenticator)
    workspace_id = assistant_setup.init_skill(assistant)
    socketio.run(app, host='localhost', port=int(port))
